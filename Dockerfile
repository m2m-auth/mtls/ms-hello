FROM alpine
RUN apk add openjdk11
RUN apk add openssl

RUN adduser appuser -D

WORKDIR /home/appuser

COPY server.ext server.ext
COPY client.ext client.ext
COPY target/ms-hello.jar app.jar
COPY start.sh start.sh

CMD ["./start.sh"]
