#!/bin/sh

# Create root CA
openssl req -x509 -sha256 -days 3650 -newkey rsa:4096 -keyout rootCA.key -out rootCA.crt -passin pass:123456 -passout pass:123456 -subj "/C=BE/ST=Brussels/L=Brussels/O=Global Security/OU=IT Department/CN=Nicholas Meyers"

# Create Certificate request for server
openssl req -new -newkey rsa:4096 -keyout server.key -out server.csr  -passin pass:123456 -passout pass:123456 -subj "/C=BE/ST=Brussels/L=Brussels/O=Global Security/OU=IT Department/CN=ms-hello"

# Create Certificate request for client
openssl req -new -newkey rsa:4096 -keyout client.key -out client.csr  -passin pass:123456 -passout pass:123456 -subj "/C=BE/ST=Brussels/L=Brussels/O=Global Security/OU=IT Department/CN=client"

# Sign Certificate request for server
openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in server.csr -out server.crt -days 365 -CAcreateserial -extfile server.ext -passin pass:123456

# Sign Certificate request for client
openssl x509 -req -CA rootCA.crt -CAkey rootCA.key -in client.csr -out client.crt -days 365 -CAcreateserial -extfile client.ext -passin pass:123456


# Create keystore for server
openssl pkcs12 -export -out server.p12 -name "server" -inkey server.key -in server.crt -passin pass:123456 -passout pass:123456
keytool -importkeystore -srckeystore server.p12 -srcstoretype PKCS12 -destkeystore keystore-server.jks -deststoretype JKS -storepass 123456 -keypass 123456 -srcstorepass 123456

# Create keystore for client
openssl pkcs12 -export -out client.p12 -name "client" -inkey client.key -in client.crt -passin pass:123456 -passout pass:123456
keytool -importkeystore -srckeystore client.p12 -srcstoretype PKCS12 -destkeystore keystore-client.jks -deststoretype JKS -storepass 123456 -keypass 123456 -srcstorepass 123456


# Create truststore
keytool -keystore truststore.jks -alias first -import -file rootCA.crt -storepass 123456 -noprompt


java -Duser.timezone=Europe/Brussels -jar app.jar