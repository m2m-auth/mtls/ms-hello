package be.nicholas.mtls.hello;

import org.apache.catalina.Globals;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.AbstractAccessLogValve;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.security.cert.X509Certificate;

public class AccessLog extends AbstractAccessLogValve {
    private final static Logger log = LoggerFactory.getLogger(AccessLog.class);

    @Override
    protected void log(CharArrayWriter charArrayWriter) {

    }

    @Override
    public void invoke(Request request, Response response) throws IOException, ServletException {
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute(Globals.CERTIFICATES_ATTR);
        if (certs != null) {
            for (X509Certificate cert: certs) {
                String issuer = cert.getIssuerX500Principal().getName();
                String owner = cert.getSubjectX500Principal().getName();

                log.info("Cert info[]: connector={} --- issuer={} --- owner={} --- certs={}",
                        request.getConnector(), issuer, owner, certs.length);
            }
        }
        getNext().invoke(request, response);
    }
}
