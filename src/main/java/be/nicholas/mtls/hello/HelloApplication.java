package be.nicholas.mtls.hello;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
public class HelloApplication {


    public static void main(String[] args) {
        SpringApplication.run(HelloApplication.class, args);
    }

    @Bean
    public ServletWebServerFactory servletContainer() {
        AccessLog accessLog = new AccessLog();
        accessLog.setEnabled(true);

        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addAdditionalTomcatConnectors(createSslConnectorTls());
        tomcat.addAdditionalTomcatConnectors(createSslConnectorMTls());
        tomcat.addContextValves(accessLog);
        return tomcat;
    }

    private Connector createSslConnectorTls() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        File keystore = new File("keystore-server.jks");

        connector.setScheme("https");
        connector.setSecure(true);
        connector.setPort(8443);

        protocol.setSSLEnabled(true);
        protocol.setKeystoreFile(keystore.getAbsolutePath());
        protocol.setKeystorePass("123456");
        protocol.setKeyAlias("server");

        return connector;
    }

    private Connector createSslConnectorMTls() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        File keystore = new File("keystore-server.jks");
        File truststore = new File("truststore.jks");

        connector.setScheme("https");
        connector.setSecure(true);
        connector.setPort(9443);

        protocol.setSSLEnabled(true);
        protocol.setKeystoreFile(keystore.getAbsolutePath());
        protocol.setKeystorePass("123456");
        protocol.setKeyAlias("server");
        protocol.setTruststoreFile(truststore.getAbsolutePath());
        protocol.setTruststorePass("123456");
        protocol.setClientAuth("true");

        return connector;
    }

}
